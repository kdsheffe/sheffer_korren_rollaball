﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using UnityEngine.SceneManagement;
public class PlayerController : MonoBehaviour
{

	private float movementX;
	private float movementY;
	private Rigidbody rb;
	public float speed;
	public TextMeshProUGUI countText;
	public GameObject winTextObject;
	public GameObject gameoverTextObject;
	private int count;
	public GameObject platform1;
	public GameObject platform2;
	private bool gameover;
	private bool reachedend;
	private float timer;
	private bool win;
	void Start()
	{

		rb = GetComponent<Rigidbody>();

		timer = 2.5f;

		count = 0;
		gameover = false;
		win = false;
		SetCountText();

		winTextObject.SetActive(false);
		gameoverTextObject.SetActive(false);
	}
    private void Update()
    {
		if (win == true)
		{
			timer = timer - Time.deltaTime;
			winTextObject.SetActive(true);
			if (timer <= 0.0f)
			{
				SceneManager.LoadScene("Minigame");
			}
		}
		if (transform.position.y < 0 && win == false)
        {
			gameover = true;
		}
		if (gameover == true)
		{
			timer = timer - Time.deltaTime;
			gameoverTextObject.SetActive(true);
			if (timer <= 0.0f)
			{
				SceneManager.LoadScene("Minigame");
			}
		
		}

	}
    void FixedUpdate()
	{
		Vector3 movement = new Vector3(movementX, 0.0f, movementY);

		rb.AddForce(movement * speed);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Pick Up"))
		{
			other.gameObject.SetActive(false);


			count = count + 1;


			SetCountText();
		}
		else if (other.gameObject.CompareTag("Fake Up"))
        {
			other.gameObject.SetActive(false);
			count = count + 1;
			SetCountText();
			gameover = true;
		
		}
		else if (other.gameObject.CompareTag("switch1"))
		{
			if (platform1.activeInHierarchy == false)
			{
				platform1.SetActive(true);
			}
			else
            {
				platform1.SetActive(false);
            }

		}
		else if (other.gameObject.CompareTag("switch2"))
		{
			if (platform2.activeInHierarchy == false)
			{
				platform2.SetActive(true);
			}
			else
			{
				platform2.SetActive(false);
			}
		}
		if (other.gameObject.CompareTag("goal") && count >= 15 && gameover == false)
        {
			win = true;
		}
	}

	void OnMove(InputValue value)
	{
		Vector2 v = value.Get<Vector2>();

		movementX = v.x;
		movementY = v.y;
	}

	void SetCountText()
	{
		countText.text = "Count: " + count.ToString();
	}
}
